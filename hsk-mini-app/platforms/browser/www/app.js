var http = require('http');
var fs = require('fs');

var pgp = require("pg-promise")();
var db = pgp("postgres://postgres:caroline2001@localhost:3000/hsk");
var table;

/*function getTable() {
    db.many('SELECT * FROM hsk ORDER BY reference').then(function (data) {
        table = data;
        //console.log(data[i]['traduction']);
    }).catch(function (error) {
        console.log(error)
    });
    console.log(table);
}*/

var server = http.createServer(function (req, res){
    fs.readFile('index.html', 'utf8', function (error, content, table) {
        res.writeHead(200, {"Content-Type" : "text/html"});
        res.end(content);
    });
});

var io = require('socket.io').listen(server);
/*
io.sockets.on('infoCharacter', function (socket, randomInt) {

    });
});*/

io.sockets.on('connection', function (socket) {
    socket.on('myInt', function (randomInt) {
        db.many('SELECT * FROM hsk where reference = ' + randomInt).then(function (data) {
            socket.emit('yourInt', data[0]['character'], data[0]['pinyin'], data[0]['traduction']);
            //console.log(data[i]['traduction']);
        }).catch(function (error) {
            console.log(error);
        });
    });

});

server.listen(8080);
